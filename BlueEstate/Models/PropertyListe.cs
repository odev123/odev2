﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlueEstate.Models
{
    public class PropertyListe
    {
        public Ev ev;
        public List<Ev> evbenzer3;


        public PropertyListe(BlueEstateDBEntities db,int id)
        {
            ev = db.Ev.FirstOrDefault(x => x.EvID == id );
            evbenzer3 = db.Ev.Where(x => ev.Boyut >= x.Boyut - 50 && ev.Boyut <= x.Boyut + 50 && ev.EvID != x.EvID).Take(3).ToList();

        }
    }
}   