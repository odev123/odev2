﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlueEstate.Models;


namespace BlueEstate.Models
{
    public class IlanlarSehirler
    {


        public List<Ev> ev;
        public List<Sehir> sehir;


        public IlanlarSehirler(BlueEstateDBEntities db)
        {
            ev = db.Ev.ToList();
            sehir = db.Sehir.ToList();

        }
    }
}